import java.util.Scanner;
import java.text.DecimalFormat;

public class Selectex9 {

	static Scanner tecla = new Scanner(System.in);
	
	static DecimalFormat df = new DecimalFormat("0.00");
	
	public static void main(String[] args) {
	
	//Declara��o de variaveis
	double macas;
	
	//Entrada de dados
	System.out.println("Quantidade de ma��s compradas: ");
	macas = tecla.nextDouble();
	
	if(macas < 12)
		System.out.println("Valor da compra: " + df.format(macas * 0.3));
	
	else
		System.out.println("Valor da compra: " + df.format(macas * 0.25));
		

	}

}
