import java.util.Scanner;

public class Selectex5 {

	static Scanner tecla = new Scanner (System.in);
	
	public static void main(String[] args) {
	
		//declaração das variaveis
		double valor;		
		
		//Entrada de dados
		System.out.println("Insira um valor: ");
		valor = tecla.nextDouble();
		
		//Processamento de dados
		if(valor>=0)
			System.out.println("Valor positivo");
		
		else
			System.out.println("Valor negativo");
			
	}

}
