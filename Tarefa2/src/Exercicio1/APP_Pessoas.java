package Exercicio1;

import java.util.Scanner;
import java.util.ArrayList;


public class APP_Pessoas {
	
	static Scanner tecla = new Scanner(System.in);
	
	 //Constante
    static final int MAXCADASTRO = 20;
	
	//Lista de Pessoas
    static Cad_Pessoas[] lista = new Cad_Pessoas[MAXCADASTRO];
    
	//vari�vel comum
    static int index = 0;

	public static void main(String[] args) {
		int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir Pessoa");
            System.out.println("2-Listar pessoas Cadastradas");
            System.out.println("3-Sair");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirPessoa(); break;
                case 2: listarPessoas(); break;
                case 3: break;
            }
        } while (op!=3);       
    }
		
    public static void incluirPessoa(){
        //Entrada
        System.out.println("Digite o n�mero do CPF: ");
        Double cpf = tecla.nextDouble();
        
        System.out.println("Digite a Data de nacimento ddmmaaaa: ");
        int nascimento = tecla.nextInt();
        
        System.out.println("Digite o Nome: ");
        String nome = tecla.next();
              
        
        //Criar o objeto e inserir na lista
        lista[index++] = new Cad_Pessoas(cpf, nome, nascimento);
        System.out.println("Conta cadastrada com sucesso!");
    }

    public static void listarPessoas(){
        double total = 0;
        System.out.println("N� CPF:........ NOME:....... DATA NASCIMENTO:");
        for (int i = 0; i < lista.length-1; i++) {
            if (lista[i] != null){
                System.out.println(lista[i].getCpf()
                                   + "........" +
                                   lista[i].getNome()
                                   + "........" +
                                   lista[i].getNascimento());
            }else{
                break;
            }
        }
    }        
    
}


